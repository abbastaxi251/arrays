function flatten(elements, depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let flatten_array = []
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index]) && depth > 0) {
            // if (depth == undefined) {
            //     flatten_array = flatten_array.concat(flatten(elements[index]))
            // }
            // else if (depth > 0) {
            flatten_array = flatten_array.concat(flatten(elements[index], depth - 1))
            // }
            // else {
            //     if (elements[index] != undefined) {
            //         flatten_array.push(elements[index])
            //     }
            // }
        }
        else {
            if (elements[index] != undefined) {
                flatten_array.push(elements[index])
            }
        }
    }
    return flatten_array

}

module.exports = flatten